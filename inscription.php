<?php
include("./boffice/access.php");
?>
<!doctype html>
<head>
<title>Inscription Terre de Convergence</title>
<style>
* {box-sizing:border-box;}
html, body {padding:0px;margin:0px;font-family:Arial;width:100%;height:100%;}
div {padding:0px;margin:0px;font-family:Arial;width:100%;}
form {padding:0px 20px 0px 20px}
input, textarea, select {padding:4px;border-radius:6px;font-family:Arial;width:100%;font-size:1em;}
input[type=button], input[type=submit] {background-color:#fed;cursor: pointer;font-size:1em;}
input[type=radio] {padding:0px;margin:0px;border:none;display:inline-block;}
::placeholder {color:#bbb; }
td {padding:6px;}
ul, li {padding:2px;}
li {display:inline-block;}
a {color:#c00;}
h5 {text-align:center;margin:0px;padding:0px;line-height:2em;border-bottom:solid 1px #ccc;font-size:1.25em;}
.lebloc {border-radius:8px;border:solid 1px #aaa;background:#DFF0D8;}
.disblock {display:block;}
.nonblock {display:none;}
</style>
<script>
function letype(){
var x = document.getElementById("type").selectedIndex;
var y = document.getElementById("type").options;
var z = document.getElementById("typey");
//alert("Index: " + y[x].index + " is " + y[x].text);
if(y[x].index == 10) {
  tagel("typex");
  z.focus();
  z.style.borderColor = "#fc0";
}
else {
  var element = document.getElementById("typex");
  element.classList.remove("disblock");
   element.classList.add("nonblock");
  }
}
function letheme(){
var x = document.getElementById("theme").selectedIndex;
var y = document.getElementById("theme").options;
var z = document.getElementById("themey");
//alert("Index: " + y[x].index + " is " + y[x].text);
if(y[x].index == 9) {
  tagel("themex");
  z.focus();
  z.style.borderColor = "#fc0";
}
else {
  var element = document.getElementById("themex");
  element.classList.remove("disblock");
   element.classList.add("nonblock");
  }
}
function tagel(div) {
  var element = document.getElementById(div);
if(element.className == "nonblock"){
   element.classList.remove("nonblock");
   element.classList.add("disblock");
}
else{
   element.classList.remove("disblock");
   element.classList.add("nonblock");
   }
}
</script>
</head>
<body>

<div>
<br/>
<div class='lebloc' style='max-width:480px;margin:0px auto 0px auto'>
<h5>TERRE DE CONVERGENCE 2019</h5>
<form action='add_inscription.php' method='post'>
<div style='text-align:center;margin-top:.5em;'>Formulaire d'inscription de votre proposition d'atelier</div>
<br/>
Nom<br/>
<input type='text' name='nom' value="" required/>
<br/>
Pr&eacute;nom<br/>
<input type='text' name='prenom' value="" required/>
<br/>
T&eacute;l&eacute;phone<br/>
<input type='text' name='mobile' value="" required/>
<br/>
Email<br/>
<input type='text' name='email' value="" required/>
<br/>
<br/>
<!--
<select name='type' id='type' onchange='letype();'>
<?php foreach($tablotypeinsc as $cle=>$val) { echo "<option value='".$cle."'>".$val."</option>"; } ?>
</select>
<div class='nonblock' id='typex'>
Ajouter un type d'intervention<br/>
<input type='text' name='typey' id='typey' value=""/>
</div>

<br/>

<br/>

<select name='theme' id='theme' onchange='letheme();'>
<?php
foreach($tablotheme as $cle=>$val) { echo "<option value='".$cle."'>".$val."</option>"; } ?>
</select>
<div class='nonblock' id='themex'>
Ajouter un th&egrave;me<br/>
<input type='text' name='themey' id='themey' value=""/>
</div>
<br/>
-->
Nom de votre atelier (10 mots maxi)
<br/>
<input type='text' name='titre' value="" required/>
<br/>
Description (400 caract&egrave;res maxi)
<br/>
<textarea name='description' rows='6'></textarea>
<br/>
<br/>
<div style='text-align:center;'>Jour et horaire souhait&eacute;s</div>
<br/>
<table border='0' style='width:100%'>
<tr><td style='width:50%;padding:0px;'>
Jour
<br/>
<select name='jour'>
<?php
foreach($tablojourmax as $cle=>$val) { echo "<option value='".$cle."'>".$val."</option>"; } ?>
</select>
</td><td style='width:25%;padding:0px;'>
D&eacute;but
<br/>
<select name='heure'>
<?php 
foreach($tabloheure as $val) { echo "<option value='".$val."'>".$val."</option>"; } ?>
</select>
</td><td style='width:25%;padding:0px;'>
Fin
<br/>
<select name='heurefin'>
<?php 
foreach($tabloheure as $val) { echo "<option value='".$val."'>".$val."</option>"; } ?>
</select>
</td></tr>
</table>
<br/>
Autres disponibilit&eacute;s (400 caract&egrave;res maxi)
<br/>
<textarea name='dateheure' rows='4'></textarea>

<br/>
<input type='submit' value=' ENREGISTRER ' />

</form>

<br/>
<div style='text-align:center;'><i>Contact <a href='mailto:contact@rdevolution.org'>contact@rdevolution.org</a></i></div>
<br/>
</div>
</div>
<?php
if(isset($_GET['m'])&&($_GET['m']=='1')) echo "<script>alert('Inscription enregistree...');</script>";
else echo "";
?>
<br/>
</body>
</html>