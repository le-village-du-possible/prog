SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Structure de la table `datesup`
--

CREATE TABLE `datesup` (
  `cle` int(11) NOT NULL,
  `intervenant` int(11) NOT NULL,
  `datehoraire` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Structure de la table `intervenants`
--

CREATE TABLE `intervenants` (
  `cle` int(11) NOT NULL,
  `nom` varchar(250) NOT NULL,
  `prenom` varchar(250) NOT NULL,
  `mobile` varchar(25) NOT NULL,
  `email` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Structure de la table `lieux`
--

CREATE TABLE `lieux` (
  `cle` int(11) NOT NULL,
  `nom` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Structure de la table `planning`
--

CREATE TABLE `planning` (
  `cle` int(11) NOT NULL,
  `jour` tinyint(5) NOT NULL,
  `heure` varchar(5) NOT NULL,
  `heurefin` varchar(5) NOT NULL,
  `lieu` int(5),
  `theme` int(5),
  `intervenant` int(11) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `valid` tinyint(3) NOT NULL DEFAULT 1,
  `onoff` tinyint(3) NOT NULL DEFAULT 0,
  `type` int(5),
  `referent` int(5),
  `secretaire` int(5),
  `rapporteur` int(5),
  `online` int(5) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;




--
-- Structure de la table `rapporteur`
--

CREATE TABLE `rapporteur` (
  `cle` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Structure de la table `referents`
--

CREATE TABLE `referents` (
  `cle` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `mobile` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Structure de la table `secretaire`
--

CREATE TABLE `secretaire` (
  `cle` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Structure de la table `themes`
--

CREATE TABLE `themes` (
  `cle` int(11) NOT NULL,
  `nom` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Structure de la table `type`
--

CREATE TABLE `type` (
  `cle` int(11) NOT NULL,
  `nom` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Index pour la table `datesup`
--
ALTER TABLE `datesup`
  ADD UNIQUE KEY `dathor` (`cle`);

--
-- Index pour la table `intervenants`
--
ALTER TABLE `intervenants`
  ADD PRIMARY KEY (`cle`);

--
-- Index pour la table `lieux`
--
ALTER TABLE `lieux`
  ADD PRIMARY KEY (`cle`);

--
-- Index pour la table `planning`
--
ALTER TABLE `planning`
  ADD PRIMARY KEY (`cle`);

--
-- Index pour la table `rapporteur`
--
ALTER TABLE `rapporteur`
  ADD PRIMARY KEY (`cle`);

--
-- Index pour la table `referents`
--
ALTER TABLE `referents`
  ADD PRIMARY KEY (`cle`);

--
-- Index pour la table `secretaire`
--
ALTER TABLE `secretaire`
  ADD PRIMARY KEY (`cle`);

--
-- Index pour la table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`cle`);

--
-- Index pour la table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`cle`);

--
-- AUTO_INCREMENT pour la table `intervenants`
--
ALTER TABLE `intervenants`
  MODIFY `cle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT pour la table `lieux`
--
ALTER TABLE `lieux`
  MODIFY `cle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT pour la table `planning`
--
ALTER TABLE `planning`
  MODIFY `cle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT pour la table `rapporteur`
--
ALTER TABLE `rapporteur`
  MODIFY `cle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT pour la table `referents`
--
ALTER TABLE `referents`
  MODIFY `cle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT pour la table `secretaire`
--
ALTER TABLE `secretaire`
  MODIFY `cle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT pour la table `themes`
--
ALTER TABLE `themes`
  MODIFY `cle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT pour la table `type`
--
ALTER TABLE `type`
  MODIFY `cle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
