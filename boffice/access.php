<?php

$chemin = "https://nom_de_domaine.tld";

$host = "localhost";
$dbname = "nom_bdd";
$user = "user_bdd";
$pass = "pass_bdd";

$connect = mysqli_connect($host, $user, $pass, $dbname)  or die ('Identifiants incorrects');
mysqli_set_charset($connect, "utf8");


function envoiunmail($meldest,$melsub,$melbod){
        $monmail = "email@nom_de_domaine.tld";
        $boundary = uniqid("TdC2019");
	$from = "TdC 2019 <".$monmail.">";
	$to = $meldest;
	$subject = utf8_decode($melsub);
	$headers = "MIME-Version: 1.0\n".
	   "From: ".$from."\n".
	   "Reply-To: ".$monmail."\n".
//	   "Message-id: ".generateMessageID()."\n".
	   "X-Mailer: PHP/".phpversion()."\n".
	   "Date: ".date("r")."\n".
//	   "Subject: ".$subject."\n".
           "Content-Type: multipart/mixed; boundary = $boundary\n\n";
	$body = "--$boundary\n" .
	   "Content-Type: text/html; charset=UTF-8\n" .
	   "\n";
	$body .= "<body style='font-family: Roboto, \"Helvetica Neue\", Arial; font-size: 16px;'>
		  <table width='650' cellpadding='0' cellspacing='0' align='center' border='0'><tr>
		  <td><p>
		  ".nl2br(stripslashes($melbod))."</p></td></tr></table></body>";
	$body .= "\n\n--".$boundary."--";
	@mail($to, $subject, $body, $headers);
}



$tablojour = array("13","14","15","16","17","18");
$tablojourmax = array("13"=>"Mardi 13 Ao&ucirc;t","14"=>"Mercredi 14 Ao&ucirc;t","15"=>"Jeudi 15 Ao&ucirc;t","16"=>"Vendredi 16 Ao&ucirc;t","17"=>"Samedi 17 Ao&ucirc;t","18"=>"Dimanche 18 Ao&ucirc;t");
$tablojourbene = array("12","13","14","15","16","17","18","19","20","21","22");
$tablojourfinal = array();
for($i=1;$i<31;$i++){ $tablojourfinal[] = $i; }

$tablovalid = array("0"=>"Oui", "1"=>"Non");
$tabloonoff = array("0"=>"Public", "1"=>"Priv&eacute;");

$tabloheure = array("7h","7h30","8h","8h30","9h","9h30","10h","10h30","11h","11h30","12h","12h30","13h","13h30","14h","14h30","15h","15h30","16h","16h30","17h","17h30","18h","18h30","19h","19h30","20h","20h30","21h","21h30","22h","22h30","23h","23h30","0h","0h30","1h","1h30");
//for($i=8;$i<23;$i++){ $tabloheure[] = $i; }

$tablolieux = array();
     $qd = "SELECT cle,nom FROM lieux ORDER BY nom";
    $resultd = mysqli_query($connect,$qd);
    while($row = mysqli_fetch_array($resultd))
    {
      $tablolieux[$row['cle']] = $row['nom'];
    }

$tablotheme = array();
     $qd = "SELECT cle,nom FROM themes ORDER BY nom";
    $resultd = mysqli_query($connect,$qd);
    while($row = mysqli_fetch_array($resultd))
    {
      $tablotheme[$row['cle']] = $row['nom'];
    }

$tablotypeinsc = array();
     $qd = "SELECT cle,nom FROM type ORDER BY nom";
    $resultd = mysqli_query($connect,$qd);
    while($row = mysqli_fetch_array($resultd))
    {
      $tablotypeinsc[$row['cle']] = $row['nom'];
    }

$tabloreferent= array();
     $qd = "SELECT cle,nom,mobile FROM referents ORDER BY nom";
    $resultd = mysqli_query($connect,$qd);
    while($row = mysqli_fetch_array($resultd))
    {
      $tabloreferent[$row['cle']] = array("nom"=>$row['nom'],"mobile"=>$row['mobile']);
    }

$tablointervenant= array();
     $qd = "SELECT cle,nom,prenom,mobile,email FROM intervenants ORDER BY nom";
    $resultd = mysqli_query($connect,$qd);
    while($row = mysqli_fetch_array($resultd))
    {
      $tablointervenant[$row['cle']] = array("nom"=>$row['nom'],"prenom"=>$row['prenom'],"mobile"=>$row['mobile'],"email"=>$row['email']);
    }

$tablosecretaire= array();
     $qd = "SELECT cle,nom FROM secretaire ORDER BY nom";
    $resultd = mysqli_query($connect,$qd);
    while($row = mysqli_fetch_array($resultd))
    {
      $tablosecretaire[$row['cle']] = $row['nom'];
    }

$tablorapporteur= array();
     $qd = "SELECT cle,nom FROM rapporteur ORDER BY nom";
    $resultd = mysqli_query($connect,$qd);
    while($row = mysqli_fetch_array($resultd))
    {
      $tablorapporteur[$row['cle']] = $row['nom'];
    }

$tablointervenantvide= array();
     $qd = "SELECT t1.cle,t1.nom,t1.prenom,t1.mobile,t1.email FROM intervenants AS t1, planning AS t2 WHERE t1.cle=t2.intervenant ORDER BY t1.nom";
    $resultd = mysqli_query($connect,$qd);
    while($row = mysqli_fetch_array($resultd))
    {
      $tablointervenantvide[$row['cle']] = array("nom"=>$row['nom'],"prenom"=>$row['prenom'],"mobile"=>$row['mobile'],"email"=>$row['email']);
    }

?>
