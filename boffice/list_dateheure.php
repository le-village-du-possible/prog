<?php
include("./access.php");
?>
<!doctype html>
<head>
<title>Liste date/heure annexe</title>
<link rel="stylesheet" media="all" href="css.css" />
<script>
function tagel(div) {
  var element = document.getElementById("a"+div);
  var lebout = document.getElementById("b"+div);
if(element.className == "nonblock"){
   element.classList.remove("nonblock");
   element.classList.add("disblock");
   lebout.innerHTML = "moins";
}
else{
   element.classList.remove("disblock");
   element.classList.add("nonblock");
   lebout.innerHTML = "plus";
}
}
</script>
</head>
<body>
<br/>
<?php include("nav.php"); ?>
<div>


<div style='padding:1em .5em;width:50%;margin:0px auto 0px auto'>
<div class='lebloc'>
<h5>LISTE DES ATELIERS, inscriptions en ligne A VALIDER</h5>
<br/>

<?php

$malistec = "";
foreach($tablojourmax as $lejour=>$lemax){

  $malistec .= "<tr><td colspan='5' style='background:#fed;height:80%;line-height:80%;border-bottom:solid 1px #444;width:100%;'>".$lemax."</td></tr>";

     $qd = "SELECT * FROM planning WHERE jour='".$lejour."' AND valid='1' AND online='1' ORDER BY jour, CAST(heure AS UNSIGNED)";
    $resultd = mysqli_query($connect,$qd);
    while($row = mysqli_fetch_array($resultd))
    {

     $qdz = "SELECT datehoraire FROM datesup WHERE cle='".$row['cle']."'";
    $resultdz = mysqli_query($connect,$qdz);
    list($datehorairex) = mysqli_fetch_array($resultdz);

      $lemob = str_replace(" ","",$tablointervenant[$row['intervenant']]['mobile']);
      $lemob = str_replace(".","",$lemob);
      $lemob = str_replace("+33","0",$lemob);
      $dekmob = str_split($lemob, 2);
      $nummob = "";
      foreach($dekmob as $valq){ $nummob .= $valq." "; }

      $malistec .= "<tr style='border-top:solid 1px #444;'>
      <td style='width:20%'>".$row['heure']." &agrave; ".$row['heurefin']."</td>
      <td style='width:20%;".$bull."'> ".$tablointervenant[$row['intervenant']]['prenom']." ".$tablointervenant[$row['intervenant']]['nom']."</td>
      <td style='width:20%'>".$tablointervenant[$row['intervenant']]['email']."</td>
      <td style='width:20%;'>".$nummob."</td>
      <td style='width:20%;text-align:right;'><a href='index.php?cle=".$row['cle']."#".$row['cle']."'>Voir fiche</a></td>
      </tr>
      <tr>
      <td colspan='5' style='background:#fff;'><b>".$row['titre']."</b> ".$row['description']."</td>
      
      </tr>";
      if($datehorairex!='') $malistec .= "<tr><td colspan='5' style='background:#def'>".$datehorairex."</td></tr>";
    }
}
?>


<table width='100%' border='1' bordercolor='#eee' align='center' style='border-collapse:collapse;width:100%;font-size:.85em;background:#fafafa;'>
<?php
echo $malistec;
?>
</table>
<br/>
</div>
</div>

</div>
</body>
</html>