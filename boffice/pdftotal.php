<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

include("./access.php");
?>
<!doctype html>
<head>
<title>Generation des PDFs</title>
<link rel="stylesheet" media="all" href="css.css" />
</head>
<body>
<br/>
<?php include("nav.php"); ?>
<br/>
<div style='width:50%;margin:0 auto 0 auto;'>

<div class='lebloc'>
<h5>GENERER LES PDFs</h5>
<h4 style='color:#c00'>!! ATTENTION !! Fonctions gourmandes en ressources serveur. A utiliser avec parcimonie. Merci</h4>
<div style='padding:1em;'>
<br/>
<a href='gener_type.php' class='lien' target='neo'>G&eacute;n&eacute;rer les .pdf des Types</a><br/><br/>
<a href='gener_theme.php' class='lien' target='neo'>G&eacute;n&eacute;rer les .pdf des Themes</a><br/><br/>
<a href='gener_lieux.php' class='lien' target='neo'>G&eacute;n&eacute;rer les .pdf des Lieux</a><br/><br/>
<a href='gener_referent.php' class='lien' target='neo'>G&eacute;n&eacute;rer les .pdf des Referents</a><br/><br/>
<a href='generpdf.php' class='lien' target='neo'>G&eacute;n&eacute;rer le .pdf Agenda Paysage</a><br/><br/>
<a href='generpdfport.php' class='lien' target='neo'>G&eacute;n&eacute;rer le .pdf Agenda Portrait</a><br/><br/>
<a href='generbargraf.php' class='lien' target='neo'>G&eacute;n&eacute;rer le .pdf Planning Global</a><br/><br/>
<a href='pdfbruno.php' class='lien' target='neo'>G&eacute;n&eacute;rer le .pdf de Bruno avec tel/mel</a><br/><br/>
<a href='pdfbruno2.php' class='lien' target='neo'>G&eacute;n&eacute;rer le .pdf de Bruno sans tel/mel</a><br/><br/>
<a href='gener_lieux_bruno.php' class='lien' target='neo'>G&eacute;n&eacute;rer le .pdf des Lieux -> Bruno</a><br/><br/>
<p>
Le temps d'ex&eacute;cution est variable, compris entre 3 et 30 secondes. Merci d'effectuer le maximum d'op&eacute;rations et de modifications avant de g&eacute;n&eacute;rer un pack de fichiers.
</p>
</div>
</div>


</div>

</body>
</html>