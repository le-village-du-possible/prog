<!doctype html>
<head>
<title>Liste des PDFs</title>
<link rel="stylesheet" media="all" href="css.css" />

</head>
<body>
<br/>
<?php include("nav.php"); ?>
<br/>
<div style='width:50%;margin:0 auto 0 auto;'>
<h5>PDFs t&eacute;l&eacute;chargeables OU imprimables list&eacute;s par :</h5>
<p style='text-align:center;'>Pour forcer le t&eacute;l&eacute;chargement, une fois sur la page du fichier .pdf, taper "Ctrl + Maj + R" 2 fois de suite.</p>
</div>
<table align='center' style='margin-top:.5em;'>
<tr valign='top'>
<td>
<div class='lebloc'>
<h5>TYPES</h5>
<div style='padding:1em'>

<?php
$cpt1 = 0;
$mliststat = array();
if ($handlea= opendir("../pdfs/type")){
      	while (false !== ($filea = readdir($handlea))){
      		if ($filea != "." && $filea != ".." && $filea != "index.html"){
                
      		$mliststat[] = " &bull; <a href=\"../pdfs/type/".$filea."\" target='pdfx'>".$filea."</a><br/>";
                }
      	}
	closedir($handlea);
}
asort($mliststat);
foreach($mliststat as $val){
      $cpt1++;
      echo $cpt1." ".$val;
}
?>
</div>
</div>
</td>
<td>
<div class='lebloc'>
<h5>THEMES</h5>
<div style='padding:1em'>
<?php
$mliststat = array();
if ($handlea= opendir("../pdfs/theme")){
      	while (false !== ($filea = readdir($handlea))){
      		if ($filea != "." && $filea != ".." && $filea != "index.html"){
      		$mliststat[] = " &bull; <a href=\"../pdfs/theme/".$filea."\" target='pdfx'>".$filea."</a><br/>";
                }
      	}
	closedir($handlea);
}
asort($mliststat);
foreach($mliststat as $val){
      $cpt1++;
      echo $cpt1." ".$val;
}
?>
</div>
</div>
</td>
<td>
<div class='lebloc'>
<h5>LIEUX</h5>
<div style='padding:1em'>
<?php
$mliststat = array();
if ($handlea= opendir("../pdfs/lieu")){
      	while (false !== ($filea = readdir($handlea))){
      		if ($filea != "." && $filea != ".." && $filea != "index.html"){
      		$mliststat[] = " &bull; <a href=\"../pdfs/lieu/".$filea."\" target='pdfx'>".$filea."</a><br/>";
                }
      	}
	closedir($handlea);
}
asort($mliststat);
foreach($mliststat as $val){
      $cpt1++;
      echo $cpt1." ".$val;
}
?>
</div>
</div>
</td>
<td>
<div class='lebloc'>
<h5>REFERENT</h5>
<div style='padding:1em'>
<?php
$mliststat = array();
if ($handlea= opendir("../pdfs/referent")){
      	while (false !== ($filea = readdir($handlea))){
      		if ($filea != "." && $filea != ".." && $filea != "index.html"){
      		$mliststat[] = " &bull; <a href=\"../pdfs/referent/".$filea."\" target='pdfx'>".$filea."</a><br/>";
                }
      	}
	closedir($handlea);
}
asort($mliststat);
foreach($mliststat as $val){
      $cpt1++;
      echo $cpt1." ".$val;
}
?>
</div>
</div>
</td>
<td>
<div class='lebloc'>
<h5>GLOBAL</h5>
<div style='padding:1em'>
<?php
$mliststat = array();
if ($handlea= opendir("../pdf")){
      	while (false !== ($filea = readdir($handlea))){
      		if ($filea != "." && $filea != ".." && $filea != "index.html"){
      		$mliststat[] = " &bull; <a href=\"../pdf/".$filea."\" target='pdfx'>".$filea."</a><br/>";
                }
      	}
	closedir($handlea);
}
asort($mliststat);
foreach($mliststat as $val){
      $cpt1++;
      echo $cpt1." ".$val;
}
?>
</div></div>
</td>
</tr>
</table>

</body>
</html>

