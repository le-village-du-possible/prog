<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

include("./access.php");

$tabtime = array();
$listjourheure = array();

if(isset($_GET['type']) && $_GET['type']!='') $lasuite = " AND type='".$_GET['type']."' ";
elseif(isset($_GET['theme']) && $_GET['theme']!='') $lasuite = " AND theme='".$_GET['theme']."' ";
elseif(isset($_GET['lieu']) && $_GET['lieu']!='') $lasuite = " AND lieu='".$_GET['lieu']."' ";
else $lasuite = "";


     $qd = "SELECT * FROM planning WHERE onoff='0' AND valid='0' ".$lasuite." ORDER BY jour, CAST(heure AS UNSIGNED)";
     $resultd = mysqli_query($connect,$qd);
     while($row = mysqli_fetch_array($resultd))
    {
      $listjourheure[] = $row['jour']."-".$row['heure']."-".$row['heurefin']."-".$row['cle'];

      $dek = explode("h",$row['heure']);
      $hdeb = $dek[0];
      if($dek[1]!='') $mindeb = $dek[1];
      else $mindeb = 0;

      $dekfin = explode("h",$row['heurefin']);
      $hdebfin = $dekfin[0];
      if($dekfin[1]!='') $mindebfin = $dekfin[1];
      else $mindebfin = 0;

      if($dekfin[0]<='6') $lejourfin = 1+$row['jour'];
      else $lejourfin = $row['jour'];

      $tabtime[$row['cle']] = array("deb"=>mktime($hdeb, $mindeb, 0, 8, $row['jour'], 2019), "fin"=>mktime($dekfin[0], $mindebfin, 0, 8, $lejourfin, 2019),"quoi"=>$row['titre'],"ztheme"=>$tablotheme[$row['theme']],"zlieu"=>$tablolieux[$row['lieu']],"cle"=>$row['cle'],"desc"=>$row['description']);
    }

$tabfinstf = array();
$malistec = "";

foreach($tablojourmax as $cle=>$jour)
{
   $malistec .= "
   <tr style='background:#0F5010;color:#fff'>
   <td style='width:24%;font-size:14px;padding:0px 3px 0px 3px;border-top:solid 1px #aaa;border-bottom:solid 1px #aaa'><b> &bull; ".$jour."</b></td>";

   foreach($tabloheure as $heure)
   {
   if(strlen($heure)<4) $malistec .= "<td colspan='2' style='font-size:12px;padding:3px;border-top:solid 1px #aaa;border-bottom:solid 1px #aaa'>".$heure."</td>";
   }

   $malistec .= "
   </tr>
   ";

   foreach($listjourheure as $clex=>$val)
   {
      $lejour = explode("-",$val);

      $dek = explode("h",$lejour[1]);
      $hdeb = $dek[0];
      if($dek[1]!='') $mindeb = $dek[1];
      else $mindeb = 0;
      $ledebut = mktime($dek[0], $mindeb, 0, 8, $lejour[0], 2019);

      $dekfin = explode("h",$lejour[2]);
      $hdebfin = $dekfin[0];
      if($dekfin[1]!='') $mindebfin = $dekfin[1];
      else $mindebfin = 0;
      $lafin = mktime($dekfin[0], $mindebfin, 0, 8, $lejour[0], 2019);

       if($lejour[0] == $cle)
       {
         $malistec .= "
         <tr>
         ";
         $malistec .= "<td style='width:24%;text-align:right;padding:0px 3px 0px 0px;font-size:12px;'><b>".trim($tabtime[$lejour[3]]['quoi'])."</b><br/>".trim($tabtime[$lejour[3]]['ztheme'])." - <i>".trim($tabtime[$lejour[3]]['zlieu'])."</i></td>";

         foreach($tabloheure as $heure2)
         {
            $dekrange = explode("h",$heure2);

             if($dekrange[0]<='6') $lejourfinfin = 1+$lejour[0];
             else $lejourfinfin = $lejour[0];

             if($dekrange[1]!='') $mindebfinrange = $dekrange[1];
             else $mindebfinrange = 0;

             $lerange = mktime($dekrange[0], $mindebfinrange, 0, 8, $lejourfinfin, 2019);

            if(($lerange >= $tabtime[$lejour[3]]['deb'])&&($lerange < $tabtime[$lejour[3]]['fin']))
            {
              if($mindebfinrange != 0) $letitle = $dekrange[0]."h".$mindebfinrange;
              else $letitle = $dekrange[0]."h";

              if(strlen($heure2)<4) $zicol = str_replace('h','',$heure2);
              else $zicol = "&nbsp;";

            $malistec .= "
            <td class='jaune2'>".trim($zicol)."</td>
            ";
            }
            else  $malistec .= "<td class='lcase'>&nbsp;</td>";
         }
         $malistec .= "</tr>";
       }
   }
}
ob_start();
?>
<style>
* {box-sizing:border-box;}
html, body {padding:0px;margin:0px;font-family:Arial;width:100%;height:100%;}
div, p {padding:0px;margin:0px;width:100%;}
td {padding:0px;margin:0px;}
.jaune {background:#fc0;width:100%;color:#555;text-align:center;padding:0px;margin:0px;display:block;height:12px;font-size:10px;}
.jaune2 {background:#fc0;width:2%;color:#555;padding:0px;margin:0px;font-size:10px;text-align:center;}
.lcase {background:#fff;width:2%;color:#fff;padding:0px;margin:0px;font-size:10px;text-align:center;}
</style>

<page backtop="0mm" backbottom="0mm" backleft="0mm" backright="0mm">

<table border='1' bordercolor='#eee' style='border-collapse:collapse;border:solid 1px #eee;border-color:#eee;width:100%;'>
<?php echo $malistec; ?>
</table>

</page>
<?php
$content = ob_get_clean();
/*
$fichierw = "./res.txt";
$fpw=fopen("$fichierw","w+");
$fszw=filesize("$fichierw");
fseek($fpw,$fszw);
fputs($fpw,$content);
fclose($fpw);
*/

unlink("../pdf/planning.pdf");
require_once("./html2pdf.class.php");
    try
    {
        $html2pdf = new HTML2PDF("L", "A4", "fr");
        $html2pdf->pdf->SetAuthor('Terre de Convergence');
        $html2pdf->pdf->SetTitle('Agenda 2019');
        $html2pdf->pdf->SetSubject('Planning des interventions');
        $html2pdf->pdf->SetKeywords('Terre de Convergence');
        $html2pdf->setDefaultFont('helvetica', '', 14);
        $html2pdf->pdf->SetDisplayMode('real');
        $html2pdf->writeHTML($content);
        $html2pdf->Output("../pdf/planning.pdf", "F");
         }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
header("location: pdfs.php");
//header('Content-Type: application/pdf');
//readfile('../pdf/planning.pdf');
?>
