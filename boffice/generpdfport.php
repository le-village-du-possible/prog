<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
//error_reporting(0);
include("./access.php");

$malistec = "";
foreach($tablojourmax as $clef=>$lejour){

  $malistec .= "<tr style='border-bottom:solid 1px #333'><td colspan='4' style='background:#0F5010;color:#fff;border-bottom:solid 1px #666;width:700px;'> &bull; ".$lejour." 2019</td></tr>";

     $qd = "SELECT * FROM planning WHERE onoff='0' AND jour='".$clef."' AND valid='0' ORDER BY jour, CAST(heure AS UNSIGNED)";
    $resultd = mysqli_query($connect,$qd);
    while($row = mysqli_fetch_array($resultd))
    {
      $malistec .= "<tr valign='middle'>
      <td style='width:120px;'><b>".$row['heure']."</b> &agrave; <b>".$row['heurefin']."</b> <i style='font-size:9px'>".substr($lejour,0,-10)."</i></td>
      <td style='width:150px;font-size:12px;'><b style='color:#0F5010'>".$tablotheme[$row['theme']]."</b></td>
      <td style='width:230px;font-size:12px;'>".$tablointervenant[$row['intervenant']]['prenom']." ".$tablointervenant[$row['intervenant']]['nom']."</td>
      <td style='width:200px;text-align:right;font-size:12px;'><i style='color:#0F5010'>".$tablolieux[$row['lieu']]."</i></td>
      </tr>
      <tr style='border-bottom:solid 1px #0F5010'>
      <td colspan='4' style='border-bottom:solid 1px #0F5010;width:700px;color:#444;font-size:12px;'><span style='color:#aaa'>&bull;</span> <b style='color:#222;font-size:12px;'>".$row['titre']."</b> <span style='color:#aaa'>&bull;</span> <span style='color:#0c0'>".$tablotypeinsc[$row['type']]."</span> <span style='color:#aaa'>&bull;</span> ".$row['description']."</td>
      </tr>";
    }
}
ob_start();
?>
<style>
* {box-sizing:border-box;}
html, body {padding:0px;margin:0px;font-family:Arial;width:100%;height:100%;}
h5 {text-align:center;margin:0px;padding:0px;}
td {padding:3px;}
</style>

<page backtop="0mm" backbottom="0mm" backleft="0mm" backright="0mm">

<table border='1' bordercolor='#eee' style='border-collapse:collapse;border:solid 1px #eee;border-color:#eee;width:700px;'>
<tr style='border-bottom:solid 1px #0F5010'><td colspan='4' style='background:#0F5010;color:#fff;border-bottom:solid 1px #0F5010;width:700px;'>
<h5>TERRE DE CONVERGENCE - AGENDA 2019</h5>
</td></tr>
<?php echo $malistec; ?>
</table>

</page>
<?php
$content = ob_get_clean();
unlink("../pdf/agenda_a4.pdf");
require_once("./html2pdf.class.php");
    try
    {
        $html2pdf = new HTML2PDF("P", "A4", "fr");
        $html2pdf->pdf->SetAuthor('Terre de Convergence');
        $html2pdf->pdf->SetTitle('Agenda 2019');
        $html2pdf->pdf->SetSubject('Planning des interventions');
        $html2pdf->pdf->SetKeywords('Terre de Convergence');
        $html2pdf->setDefaultFont('helvetica', '', 14);
        $html2pdf->pdf->SetDisplayMode('real');
        $html2pdf->writeHTML($content);
        $html2pdf->Output("../pdf/agenda_a4.pdf", "F");
         }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
header("location: pdfs.php");
//readfile('../pdf/agenda_a4.pdf');
?>
