<?php
include("./access.php");
?>
<!doctype html>
<head>
<title>Liste Intervenants</title>
<link rel="stylesheet" media="all" href="css.css" />
</head>
<body>
<br/>
<?php include("nav.php"); ?>


<div>
<?php
$tabtotal = array();
 $qd = "SELECT intervenant,referent FROM planning GROUP BY intervenant";
     $resultd = mysqli_query($connect,$qd);
     while($row = mysqli_fetch_array($resultd))
    {
      $tabtotal[$row['intervenant']] = $row['referent'];
    }
?>
<div style='padding:1em .5em;width:80%;margin:0px auto 0px auto'>
<div class='lebloc'>
<h5>LISTE DES INTERVENANT</h5>
<h4>Sur fond orange, les intervenants sans attribution.</h4>
<br/>
<table width='100%' border='1' bordercolor='#eee' align='center' style='border-collapse:collapse;width:100%;font-size:.85em;background:#fafafa;'>
<tr style='font-weight:bold;'><td>&nbsp;</td><td>Pr&eacute;nom</td><td>Nom</td><td>T&eacute;l&eacute;phone</td><td>Email</td><td>R&eacute;f&eacute;rent</td><td>&nbsp;</td></tr>
<?php
$nbre = 0;
foreach($tablointervenant as $cle=>$val){
      if(!in_array($val, $tablointervenantvide)) $zcol = " style='background:#fed'";
      else $zcol = "";
      $lemob = str_replace(" ","",$val['mobile']);
      $lemob = str_replace(".","",$lemob);
      $lemob = str_replace("+33","0",$lemob);
      $dekmob = str_split($lemob, 2);
      $nummob = "";
      foreach($dekmob as $valq){ $nummob .= $valq." "; }
      $nbre++;
      echo "<tr".$zcol."><td>".$nbre."</td><td>".$val['prenom']."</td><td style='text-transform:uppercase;'>".strtoupper($val['nom'])."</td><td>".$nummob."</td><td>".$val['email']."</td><td>".$tabloreferent[$tabtotal[$cle]]['nom']."</td><td><a href='index.php?intervenant=".$cle."'>Voir/Mod.</a></td></tr>";
    }
?>
</table>
<br/>
</div>
</div>

</div>
</body>
</html>