<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
//error_reporting(0);
include("./access.php");

$malistec = "";
foreach($tablojourmax as $clef=>$lejour){

  $malistec .= "<tr style='border-bottom:solid 1px #333'><td colspan='4' style='background:#0F5010;color:#fff;border-bottom:solid 1px #666;width:975px;'> &bull; ".$lejour." 2019</td></tr>";

     $qd = "SELECT * FROM planning WHERE onoff='0' AND jour='".$clef."' AND valid='0' ORDER BY jour, CAST(heure AS UNSIGNED)";
    $resultd = mysqli_query($connect,$qd);
    while($row = mysqli_fetch_array($resultd))
    {
      $malistec .= "<tr>
      <td style='width:150px;'><b>".$row['heure']."</b> &agrave; <b>".$row['heurefin']."</b></td>
      <td style='width:250px;'><b style='color:#0F5010'>".$tablotheme[$row['theme']]."</b></td>
      <td style='width:375px;'>".$tablointervenant[$row['intervenant']]['prenom']." ".$tablointervenant[$row['intervenant']]['nom']."</td>
      <td style='width:200px;text-align:right'><i style='color:#0F5010'>".$tablolieux[$row['lieu']]."</i></td>
      </tr>
      <tr style='border-bottom:solid 1px #0F5010'>
      <td colspan='4' style='border-bottom:solid 1px #0F5010;width:975px;color:#444;font-size:12px;'><span style='color:#aaa'>&bull;</span> <b style='color:#222;font-size:12px;'>".$row['titre']."</b> <span style='color:#aaa'>&bull;</span> <span style='color:#0c0'>".$tablotypeinsc[$row['type']]."</span> <span style='color:#aaa'>&bull;</span> ".$row['description']."</td>
      </tr>";
    }
}
ob_start();
?>
<style>
* {box-sizing:border-box;}
html, body {padding:0px;margin:0px;font-family:Arial;width:100%;height:100%;}
h5 {text-align:center;margin:0px;padding:0px;}
td {padding:6px;}
</style>

<page backtop="0mm" backbottom="0mm" backleft="0mm" backright="5mm">

<table border='1' bordercolor='#eee' style='border-collapse:collapse;border:solid 1px #eee;border-color:#eee;width:975px;'>
<tr style='border-bottom:solid 1px #0F5010'><td colspan='4' style='background:#0F5010;color:#fff;border-bottom:solid 1px #0F5010;width:975px;'>
<h5>TERRE DE CONVERGENCE - AGENDA 2019</h5>
</td></tr>
<?php echo $malistec; ?>
</table>

</page>
<?php
$content = ob_get_clean();
unlink("../pdf/agenda.pdf");
require_once("./html2pdf.class.php");
    try
    {
        $html2pdf = new HTML2PDF("L", "A4", "fr");
        $html2pdf->pdf->SetAuthor('Terre de Convergence');
        $html2pdf->pdf->SetTitle('Agenda 2019');
        $html2pdf->pdf->SetSubject('Planning des interventions');
        $html2pdf->pdf->SetKeywords('Terre de Convergence');
        $html2pdf->setDefaultFont('helvetica', '', 14);
        $html2pdf->pdf->SetDisplayMode('real');
        $html2pdf->writeHTML($content);
        $html2pdf->Output("../pdf/agenda.pdf", "F");
         }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
header("location: pdfs.php");
//readfile('../pdf/agenda.pdf');
?>
