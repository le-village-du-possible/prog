<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

include("./access.php");

$malistec = "";
foreach($tablojourmax as $clef=>$lejour){

  $malistec .= "<tr style='border-bottom:solid 1px #333'><td style='background:#0F5010;color:#fff;border-bottom:solid 1px #666;width:650px;'> &bull; ".$lejour." 2019</td></tr>";

     $qd = "SELECT * FROM planning WHERE onoff='0' AND jour='".$clef."' AND valid='0' AND theme='3' ORDER BY jour, CAST(heure AS UNSIGNED)";
    $resultd = mysqli_query($connect,$qd);
    while($row = mysqli_fetch_array($resultd))
    {
      $malistec .= "<tr style='border-bottom:dotted 1px #0F5010'>
      <td>
      <u>Animateur</u> ".strtoupper($tablointervenant[$row['intervenant']]['nom'])." ".$tablointervenant[$row['intervenant']]['prenom']."
      <br/>
      <u>Atelier</u> <b>".$row['titre']."</b>
      <br/>
      <u>Descriptif</u> ".$row['description']."
      <br/>
      <u>Date / horaire / lieu</u> <i>le ".$lejour." / de ".$row['heure']." &agrave; ".$row['heurefin']." / &agrave; ".$tablolieux[$row['lieu']]."</i>
      </td>
      </tr>";
    }
}
ob_start();
?>
<style>
* {box-sizing:border-box;}
html, body {padding:0px;margin:0px;font-family:Arial;width:100%;height:100%;}
h5 {text-align:center;margin:0px;padding:0px;}
td {padding:6px;}
</style>

<page backtop="0mm" backbottom="0mm" backleft="0mm" backright="2mm">

<table border='1' bordercolor='#eee' style='border-collapse:collapse;border:solid 1px #eee;border-color:#eee;width:650px;'>
<tr style='border-bottom:solid 1px #0F5010'><td style='background:#0F5010;color:#fff;border-bottom:solid 1px #0F5010;width:650px;'>
<h5>TERRE DE CONVERGENCE - AGENDA 2019</h5>
</td></tr>
<?php echo $malistec; ?>
</table>

</page>
<?php
$content = ob_get_clean();
unlink("../pdf/agenda_bruno_sans.pdf");
require_once("./html2pdf.class.php");
    try
    {
        $html2pdf = new HTML2PDF("P", "A4", "fr");
        $html2pdf->pdf->SetAuthor('Terre de Convergence');
        $html2pdf->pdf->SetTitle('Agenda 2019');
        $html2pdf->pdf->SetSubject('Planning des interventions');
        $html2pdf->pdf->SetKeywords('Terre de Convergence');
        $html2pdf->setDefaultFont('helvetica', '', 14);
        $html2pdf->pdf->SetDisplayMode('real');
        $html2pdf->writeHTML($content);
        $html2pdf->Output("../pdf/agenda_bruno_sans.pdf", "F");
         }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
header("location: pdfs.php");
//header('Content-Type: application/pdf');
//readfile('../pdf/agenda_bruno_sans.pdf');
?>
