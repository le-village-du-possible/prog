<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

include("./boffice/access.php");

$cpte = 0;
$tabtime = array();
$listjourheure = array();

$letabdesjours = array(1=>"Lun.",2=>"Mardi",3=>"Mercredi",4=>"Jeudi",5=>"Vendredi",6=>"Samedi",7=>"Dimanche");

if(isset($_GET['type']) && $_GET['type']!='') $lasuite = " AND type='".$_GET['type']."' ";
elseif(isset($_GET['theme']) && $_GET['theme']!='') $lasuite = " AND theme='".$_GET['theme']."' ";
elseif(isset($_GET['lieu']) && $_GET['lieu']!='') $lasuite = " AND lieu='".$_GET['lieu']."' ";
elseif(isset($_GET['interv']) && $_GET['interv']!='') $lasuite = " AND intervenant='".$_GET['interv']."' ";
elseif(isset($_GET['refer']) && $_GET['refer']!='') $lasuite = " AND referent='".$_GET['refer']."' ";
else $lasuite = "";


     $qd = "SELECT * FROM planning WHERE onoff='0' AND valid='0' ".$lasuite." ORDER BY jour, CAST(heure AS UNSIGNED)";
     $resultd = mysqli_query($connect,$qd);
     while($row = mysqli_fetch_array($resultd))
    {
      $cpte++;
      $listjourheure[] = $row['jour']."-".$row['heure']."-".$row['heurefin']."-".$row['cle'];

      $dek = explode("h",$row['heure']);
      $hdeb = $dek[0];
      if($dek[1]!='') $mindeb = $dek[1];
      else $mindeb = 0;

      $dekfin = explode("h",$row['heurefin']);
      $hdebfin = $dekfin[0];
      if($dekfin[1]!='') $mindebfin = $dekfin[1];
      else $mindebfin = 0;

      if($dekfin[0]<='6') $lejourfin = 1+$row['jour'];
      else $lejourfin = $row['jour'];


      $tabtime[$row['cle']] = array("deb"=>mktime($hdeb, $mindeb, 0, 8, $row['jour'], 2019), "fin"=>mktime($dekfin[0], $mindebfin, 0, 8, $lejourfin, 2019),"quoi"=>$row['titre'],"ztheme"=>$tablotheme[$row['theme']],"zlieu"=>$tablolieux[$row['lieu']],"cle"=>$row['cle'],"desc"=>$row['description'],"cpte"=>$cpte,"interv"=>$tablointervenant[$row['intervenant']]['prenom']." ".$tablointervenant[$row['intervenant']]['nom'],"ztype"=>$tablotypeinsc[$row['type']]);
    }

$tabfinstf = array();
$malistec = "";

foreach($tablojourmax as $cle=>$jour)
{
   $malistec .= "<tr style='background:#0F5010;'><td style='width:24%;font-size:.85em;color:#fff;'><b>".$jour."</b></td>
   ";

   foreach($tabloheure as $heure)
   {
   if(strlen($heure)<4) $malistec .= "<td colspan='2' style='font-size:.85em;color:#fff;'>".$heure."</td>
   ";
   }

   $malistec .= "
   </tr>
   ";

   foreach($listjourheure as $clex=>$val)
   {
      $lejour = explode("-",$val);

      $dek = explode("h",$lejour[1]);
      $hdeb = $dek[0];
      if($dek[1]!='') $mindeb = $dek[1];
      else $mindeb = 0;
      $ledebut = mktime($dek[0], $mindeb, 0, 8, $lejour[0], 2019);

      $dekfin = explode("h",$lejour[2]);
      $hdebfin = $dekfin[0];
      if($dekfin[1]!='') $mindebfin = $dekfin[1];
      else $mindebfin = 0;
      $lafin = mktime($dekfin[0], $mindebfin, 0, 8, $lejour[0], 2019);

       if($lejour[0] == $cle)
       {
         $malistec .= "<tr class='gosho' style='font-size:.85em;'>
         ";                        
       // onclick='montreMoi(".$tabtime[$lejour[3]]['cle'].");' <div class='nonblock' id='".$tabtime[$lejour[3]]['cle']."'></div> //
       // tooltip-persistent
       $zede = date("N",$tabtime[$lejour[3]]['deb']);
         $malistec .= "<td class='gosh' tooltip=\"".strtoupper($tabtime[$lejour[3]]['interv'])."
  &bull; le ".$letabdesjours[$zede]." ".date("d",$tabtime[$lejour[3]]['deb'])." de ".date("H\hi",$tabtime[$lejour[3]]['deb'])." &agrave; ".date("H\hi",$tabtime[$lejour[3]]['fin'])."
  &bull; ".$tabtime[$lejour[3]]['ztype']."
  &bull; ".str_replace('"','',$tabtime[$lejour[3]]['desc'])."\"><span class='bord'>".$tabtime[$lejour[3]]['cle']."</span> <b>".$tabtime[$lejour[3]]['quoi']."</b><br/>".$tabtime[$lejour[3]]['ztheme']." - <i>".$tabtime[$lejour[3]]['zlieu']."</i></td>
         ";

         foreach($tabloheure as $heure2)
         {
            $dekrange = explode("h",$heure2);

             if($dekrange[0]<='6') $lejourfinfin = 1+$lejour[0];
             else $lejourfinfin = $lejour[0];


            if($dekrange[1]!='') $mindebfinrange = $dekrange[1];
            else $mindebfinrange = 0;
            $lerange = mktime($dekrange[0], $mindebfinrange, 0, 8, $lejourfinfin, 2019);

            if(($lerange >= $tabtime[$lejour[3]]['deb'])&&($lerange < $tabtime[$lejour[3]]['fin']))
            {
              if($mindebfinrange != '0') $letitle = $dekrange[0]."h".$mindebfinrange;
              else $letitle = $dekrange[0]."h";
            $malistec .= "<td class='lcase' title='".$letitle."'><div class='jaune'>.</div></td>";
            }
            else  $malistec .= "<td class='lcase'>.</td>";
         }
         $malistec .= "</tr>
         ";
       }
   }
}

?>
<!DOCTYPE html>
<html lang='fr'>
<head>
<title>Agenda graphique</title>

<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta name="description" content="terre de convergence, planning graphique, 2019"/>
	<meta name="keywords" content="terre de convergence, planning graphique"/>
	<meta name="author" content="St�phan Gelberger"/>
        <link rel="canonical" href="https://tdc.claix.com"/>

<style>
* {box-sizing:border-box;}
html, body {padding:0px;margin:0px;font-family:Arial;width:100%;height:100%;}
div {padding:0px;margin:0px;font-family:Arial;width:100%;}
form {padding:0px 20px 20px 20px}
input, textarea, select {padding:4px;border-radius:6px;font-family:Arial;width:100%;font-size:1em;}
input[type=button], input[type=submit] {background-color:#fff;cursor: pointer;font-size:1em;}
td {padding:3px;}
h5 {text-align:center;margin:0px;padding:0px;line-height:2em;border-bottom:solid 1px #ccc}
.disblock {display:block;}
.nonblock {display:none;}
.gosh {width:24%;text-align:right;}
.gosho {cursor:pointer;}
.gosho:hover {background:#eee;}
/*
.jaune {background:linear-gradient(#fff 20%, #fc0, #fff 80%);width:2%;color:#fc0;cursor:pointer;}
*/
.jaune {background:linear-gradient(#fc0 10%, #f90, #fc0 90%);width:100%;color:#fc0;padding:0;margin:0px;display:block;font-size:10px;}

.lcase {background:#fff;width:2%;color:#fff;padding:0px;margin:0px;}

[tooltip]:after {content: attr(tooltip);position: absolute;opacity: 0;transition: all 0.15s ease;padding:1em;color:#222;border-radius: 6px;box-shadow: 2px 2px 1px silver;width:500px;white-space:pre-wrap;}
[tooltip]:hover:after {opacity: 1;text-align:left;width:500px;background: #fed;margin-top:-30px;margin-left:20px;}

.bord {writing-mode:vertical-lr;text-orientation:sideways;width:20px;height:32px;color:#ddd;display:block;float:left;text-align:center;}

</style>

</head>
<body>
<br/>
<?php /* include("./boffice/nav.php"); */ ?>

<h3 style='text-align:center;'>TERRE DE CONVERGENCE - AGENDA 2019</h3>
<div>
<div style='width:75%;margin:0px auto 0px auto'>
<table border='0' align='center'><tr><td><select name='type' id='type' onchange='if (this.value) window.location.href=this.value'>
<option value='index.php'>Type d'intervention</option>
<option value='index.php'> --- Vider la s&eacute;lection --- </option>
<?php foreach($tablotypeinsc as $cle=>$val) { echo "<option value='index.php?type=".$cle."'>".$val."</option>"; } ?>
</select>
</td>
<td><select name='theme' id='theme' onchange='if (this.value) window.location.href=this.value'>
<option value='index.php'>Th&egrave;me</option>
<option value='index.php'> --- Vider la s&eacute;lection --- </option>
<?php foreach($tablotheme as $cle=>$val) { echo "<option value='index.php?theme=".$cle."'>".$val."</option>"; } ?>
</select>
</td>
<td><select name='lieu' id='lieu' onchange='if (this.value) window.location.href=this.value'>
<option value='index.php'>Lieu</option>
<option value='index.php'> --- Vider la s&eacute;lection --- </option>
<?php foreach($tablolieux as $cle=>$val) { echo "<option value='index.php?lieu=".$cle."'>".$val."</option>"; } ?>
</select>
</td>
<td><select name='interv' id='type' onchange='if (this.value) window.location.href=this.value'>
<option value='index.php'>Intervenant</option>
<option value='index.php'> --- Vider la s&eacute;lection --- </option>
<?php foreach($tablointervenant as $cle=>$val) { echo "<option value='index.php?interv=".$cle."'>".$val['nom']." ".$val['prenom']."</option>"; } ?>
</select>
</td>
<td><select name='refer' id='type' onchange='if (this.value) window.location.href=this.value'>
<option value='index.php'>R&eacute;f&eacute;rent</option>
<option value='index.php'> --- Vider la s&eacute;lection --- </option>
<?php foreach($tabloreferent as $cle=>$val) { echo "<option value='index.php?refer=".$cle."'>".$val['nom']."</option>"; } ?>
</select>
</td>
<td><a href='index.php' style='text-decoration:none;'><input type='button' style='background:#fed;' value=" Tout l'agenda "></a>
</tr>
<tr><td colspan='6' align='center'><b>
<?php 
if(isset($_GET['type']) && ($_GET['type']!='')) echo "Type : ".$tablotypeinsc[$_GET['type']];
if(isset($_GET['theme']) && ($_GET['theme']!='')) echo "Th&egrave;me : ".$tablotheme[$_GET['theme']];
if(isset($_GET['lieu']) && ($_GET['lieu']!='')) echo "Lieu : ".$tablolieux[$_GET['lieu']];
if(isset($_GET['interv']) && ($_GET['interv']!='')) echo "Intervenant : ".$tablointervenant[$_GET['interv']]['prenom']." ".$tablointervenant[$_GET['interv']]['nom'];
if(isset($_GET['refer']) && ($_GET['refer']!='')) echo "R&eacute;f&eacute;rent : ".$tabloreferent[$_GET['refer']]['nom'];
?>
</b></td></tr>
</table>
</div>

</div>

<table border='1' align='center' bordercolor='#ccc' style='border-collapse:collapse;width:95%;border-color:#ccc;'>
<tr><?php echo $malistec; ?></tr>
</table>
<br/>
<br/>
</body>
</html>
